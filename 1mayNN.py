#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import csv
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
get_ipython().run_line_magic('matplotlib', 'inline')

import matplotlib.pyplot as plt
import numpy as np
import random
import sys
import io
import os
import datetime
from bs4 import BeautifulSoup
import requests as rq
from tensorflow.keras.callbacks import LambdaCallback
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, LSTM
from tensorflow.keras.optimizers import RMSprop
from tensorflow import keras
from pyspark.sql import functions as f

#from Text import *
#from LSTM_class import *
from keras import layers, models, optimizers


# In[2]:


import os
current_path = os.getcwd()


# In[3]:


print(current_path)


# In[4]:


df = pd.read_csv("all-the-news-2-1.csv", usecols = ["title","publication"])


# In[5]:


df


# In[6]:


df.info()


# In[7]:


TMZ = df[df["publication"] == "TMZ"]
TMZ = TMZ["title"]
TMZ


# In[8]:


import matplotlib.pyplot as plt


# In[9]:


plt.hist([len(title.split()) for title in TMZ])


# In[10]:


maxwords = 5000
#vocab size


# ***TMZ***

# In[156]:


TMZwords = []
for title in TMZ:
    title = title + " ENDLENDL"
    #add an end token to each of the titles
    TMZwords.append(title)
    
# init the tokenizer with a out_of_vocabulary token 
tokenizerTMZ = Tokenizer(oov_token="<OOV>")
tokenizerTMZ.num_words = maxwords
# generate word indexes
tokenizerTMZ.fit_on_texts(TMZwords)

# generate sequences and apply padding
sequencesTMZ = tokenizerTMZ.texts_to_sequences(TMZwords)
#TODO: reduce vocab to 10k 
TMZset = set(i for j in sequencesTMZ for i in j)


# In[157]:


tokenizerTMZ.word_index


# In[158]:


len(TMZset)


# In[159]:


OOVcount = []
for seq in sequencesTMZ:
    OOVcount.append( list(seq).count(1) )


# In[160]:


OOVcount.count(0)


# In[161]:


print(len(OOVcount))


# In[162]:


seqlen = 4
step = 1
TMZtokens = []
#TMZf = paddedTMZ[:10000].flatten()
for x, seq in enumerate(sequencesTMZ):
    if(OOVcount[x] > 0):
        continue
    if(len(seq)<seqlen + 1):
        continue
    for i in range(0, len(seq) - seqlen + 1, step):
        TMZtokens.append(seq[i: i + seqlen])


# In[163]:


print(sequencesTMZ[5:10])
TMZtokens[:10]


# In[164]:


TMZx = np.zeros((len(TMZtokens), seqlen-1), dtype=int)
TMZy = np.zeros((len(TMZtokens), 1), dtype=int)
print(TMZx.shape)
for i, seq in enumerate(TMZtokens):
    TMZx[i] = seq[:seqlen - 1]
    TMZy[i] = seq[-1]


# In[165]:


TMZm = int(len(TMZy)*0.7)
print(TMZm)
TMZ_xtest = TMZx[:TMZm]
TMZ_ytest = TMZy[:TMZm]
TMZ_xtrain = TMZx[TMZm:]
TMZ_ytrain = TMZy[TMZm:]


# In[166]:


def TMZlstm_model(sequence_length, vocab_size, layer_size):
    model = models.Sequential()
    model.add(layers.Embedding(vocab_size, layer_size))
    model.add(layers.Dropout(0.4))
    model.add(layers.LSTM(layer_size))    
    model.add(layers.Dropout(0.4))
    model.add(layers.Dense(vocab_size, activation='softmax'))
    return model


# In[167]:


TMZmodel = TMZlstm_model(seqlen, len(TMZset), 32)
optimizer = optimizers.Adam(learning_rate=0.01)
TMZmodel.compile(loss='sparse_categorical_crossentropy', optimizer=optimizer)


# In[169]:


TMZmodel.summary()


# In[ ]:


historyTMZ = TMZmodel.fit(TMZ_xtrain, TMZ_ytrain,
             validation_data=(TMZ_xtest, TMZ_ytest),
             epochs=10,
             verbose=1)


# In[ ]:


plt.plot(historyTMZ.history['loss'])
plt.plot(historyTMZ.history['val_loss'])


# In[ ]:


tokenizerTMZ.word_index


# In[ ]:


TMZmodel.save("/Users/megha/tf")


# In[ ]:


TMZs = tokenizerTMZ.texts_to_sequences(["The cops seen"])
TMZinput = np.array(TMZs)
maxTMZ = 0
while(maxTMZ != 2):
    print(TMZinput)
    pred = TMZmodel.predict(TMZinput)
    #print(tokenizerTMZ.sequences_to_texts([0]))
    maxTMZ = np.argmax(pred)
    print(tokenizerTMZ.sequences_to_texts([[maxTMZ]]))
    TMZinput = np.append(TMZinput[0,1:], maxTMZ).reshape(1,seqlen-1)


# **FOX NEWS**

# In[11]:


Fox = df[df["publication"] == "Fox News"]
Fox = Fox["title"]
print(Fox)


# In[12]:


plt.hist([len(title.split()) for title in Fox])


# In[13]:


maxwords = 5000


# In[14]:


Foxwords = []
for title in Fox:
    title = title + " ENDLENDL"
    #add an end token to each of the titles
    Foxwords.append(title)
    
# init the tokenizer with a out_of_vocabulary token 
tokenizerFox = Tokenizer(oov_token="<OOV>")
tokenizerFox.num_words = maxwords
# generate word indexes
tokenizerFox.fit_on_texts(Foxwords)

# generate sequences and apply padding
sequencesFox = tokenizerFox.texts_to_sequences(Foxwords)
#TODO: reduce vocab to 10k 
Foxset = set(i for j in sequencesFox for i in j)


# In[15]:


tokenizerFox.word_index


# In[16]:


len(Foxset)


# In[17]:


OOVcount = []
for seq in sequencesFox:
    OOVcount.append( list(seq).count(1) )


# In[18]:


OOVcount.count(0)


# In[19]:


print(len(OOVcount))


# In[20]:


seqlen = 4
step = 1
Foxtokens = []

for x, seq in enumerate(sequencesFox):
    if(OOVcount[x] > 0):
        continue
    if(len(seq)<seqlen + 1):
        continue
    for i in range(0, len(seq) - seqlen + 1, step):
        Foxtokens.append(seq[i: i + seqlen])


# In[21]:


print(sequencesFox[:10])
Foxtokens[:10]


# In[22]:


Foxx = np.zeros((len(Foxtokens), seqlen-1), dtype=int)
Foxy = np.zeros((len(Foxtokens), 1), dtype=int)
print(Foxx.shape)
for i, seq in enumerate(Foxtokens):
    Foxx[i] = seq[:seqlen - 1]
    Foxy[i] = seq[-1]


# In[23]:


Foxm = int(len(Foxy)*0.7)
print(Foxm)
Fox_xtest = Foxx[:Foxm]
Fox_ytest = Foxy[:Foxm]
Fox_xtrain = Foxx[Foxm:]
Fox_ytrain = Foxy[Foxm:]


# In[24]:


def Foxlstm_model(sequence_length, vocab_size, layer_size):
    model = models.Sequential()
    model.add(layers.Embedding(vocab_size, layer_size))
    model.add(layers.Dropout(0.4))
    model.add(layers.LSTM(layer_size))    
    model.add(layers.Dropout(0.4))
    model.add(layers.Dense(vocab_size, activation='softmax'))
    return model


# In[25]:


Foxmodel = Foxlstm_model(seqlen, len(Foxset), 32)
optimizer = optimizers.Adam(learning_rate=0.01)
Foxmodel.compile(loss='sparse_categorical_crossentropy', optimizer=optimizer)


# In[26]:


historyFox = Foxmodel.fit(Fox_xtrain, Fox_ytrain,
             validation_data=(Fox_xtest, Fox_ytest),
             epochs=10,
             verbose=1)


# In[27]:


plt.plot(historyFox.history['loss'])
plt.plot(historyFox.history['val_loss'])


# In[28]:


tokenizerFox.word_index


# In[143]:


Foxs = tokenizerFox.texts_to_sequences(["Joe Biden wins"])
Foxinput = np.array(Foxs)
for i in range(10):
    pred = Foxmodel.predict(Foxinput)
    #print(tokenizerTMZ.sequences_to_texts([0]))
    maxFox = np.argmax(pred)
    print(tokenizerFox.sequences_to_texts([[maxFox]]))
    Foxinput = np.append(Foxinput[0,1:], maxFox).reshape(1,seqlen-1)


# In[ ]:


Foxmodel.save("/Users/megha/tf")


# **NYT**

# In[146]:


NYT = df[df["publication"] == "The New York Times"]
NYT = NYT["title"]
NYT


# In[147]:


plt.hist([len(str(title).split()) for title in NYT])


# In[148]:


NYTwords = []
for title in NYT:
    title = str(title)
    title = title + " ENDLENDL"
    #add an end token to each of the titles
    NYTwords.append(title)
    
# init the tokenizer with a out_of_vocabulary token 
tokenizerNYT = Tokenizer(oov_token="<OOV>")
tokenizerNYT.num_words = maxwords
# generate word indexes
tokenizerNYT.fit_on_texts(NYTwords)

# generate sequences
sequencesNYT = tokenizerNYT.texts_to_sequences(NYTwords)
#TODO: reduce vocab to 10k 
NYTset = set(i for j in sequencesNYT for i in j)


# In[149]:


len(NYTset)


# In[150]:


OOVcount = []
for seq in sequencesNYT:
    OOVcount.append( list(seq).count(1) )


# In[151]:


OOVcount.count(0)


# In[152]:


print(len(OOVcount))


# In[153]:


seqlen = 4
step = 1
NYTtokens = []
#NYTf = paddedNYT[:10000].flatten()
for x, seq in enumerate(sequencesNYT):
    if(OOVcount[x] > 0):
        continue
    if(len(seq)<seqlen + 3):
        continue
    for i in range(0, len(seq) - seqlen + 1, step):
        NYTtokens.append(seq[i: i + seqlen])


# In[ ]:


print(sequencesNYT[5:10])
NYTtokens[:10]


# In[ ]:


NYTx = np.zeros((len(NYTtokens), seqlen-1), dtype=int)
NYTy = np.zeros((len(NYTtokens), 1), dtype=int)
print(NYTx.shape)
for i, seq in enumerate(NYTtokens):
    NYTx[i] = seq[:seqlen - 1]
    NYTy[i] = seq[-1]


# In[ ]:


NYTm = int(len(NYTy)*0.7)
print(NYTm)
NYT_xtest = NYTx[:NYTm]
NYT_ytest = NYTy[:NYTm]
NYT_xtrain = NYTx[NYTm:]
NYT_ytrain = NYTy[NYTm:]


# In[ ]:


def NYTlstm_model(sequence_length, vocab_size, layer_size):
    model = models.Sequential()
    model.add(layers.Embedding(vocab_size, layer_size))
    model.add(layers.Dropout(0.4))
    model.add(layers.LSTM(layer_size))    
    model.add(layers.Dropout(0.4))
    model.add(layers.Dense(vocab_size, activation='softmax'))
    return model


# In[ ]:


NYTmodel = NYTlstm_model(seqlen, len(NYTset), 32)
optimizer = optimizers.Adam(learning_rate=0.01)
NYTmodel.compile(loss='sparse_categorical_crossentropy', optimizer=optimizer)


# In[ ]:


historyNYT = NYTmodel.fit(NYT_xtrain, NYT_ytrain,
             validation_data=(NYT_xtest, NYT_ytest),
             epochs=5,
             verbose=1)


# In[ ]:


plt.plot(historyNYT.history['loss'])
plt.plot(historyNYT.history['val_loss'])


# In[154]:


tokenizerNYT.word_index


# In[ ]:


NYTs = tokenizerNYT.texts_to_sequences([""])
NYTinput = np.array(NYTs)
maxNYT = 0
while (maxNYT!=2):
    pred = NYTmodel.predict(NYTinput)
    #print(tokenizerNYT.sequences_to_texts([0]))
    maxNYT = np.argmax(pred)
    print(tokenizerNYT.sequences_to_texts([[maxNYT]]))
    NYTinput = np.append(NYTinput[0,1:], maxNYT).reshape(1,seqlen-1)


# In[ ]:


NYTmodel.save("/Users/megha/tf")


# ***Reuters***

# In[ ]:


REUTERS = df[df["publication"] == "Reuters"]
REUTERS = REUTERS["title"]
print(REUTERS)


# In[ ]:


plt.hist([len(title.split()) for title in REUTERS])


# In[ ]:


maxwords = 5000


# In[ ]:


REUTERSwords = []
for title in REUTERS:
    title = title + " ENDLENDL"
    #add an end token to each of the titles
    REUTERSwords.append(title)
    
# init the tokenizer with a out_of_vocabulary token 
tokenizerREUTERS = Tokenizer(oov_token="<OOV>")
tokenizerREUTERS.num_words = maxwords
# generate word indexes
tokenizerREUTERS.fit_on_texts(REUTERSwords)

# generate sequences and apply padding
sequencesREUTERS = tokenizerREUTERS.texts_to_sequences(REUTERSwords)
#TODO: reduce vocab to 10k 
REUTERSset = set(i for j in sequencesREUTERS for i in j)


# In[ ]:


tokenizerREUTERS.word_index


# In[ ]:


len(REUTERSset)


# In[ ]:


OOVcount = []
for seq in sequencesREUTERS:
    OOVcount.append( list(seq).count(1) )


# In[ ]:


OOVcount.count(0)


# In[ ]:


print(len(OOVcount))


# In[ ]:


seqlen = 4
step = 1
REUTERStokens = []

for x, seq in enumerate(sequencesREUTERS):
    if(OOVcount[x] > 0):
        continue
    if(len(seq)<seqlen + 8):
        continue
    for i in range(0, len(seq) - seqlen + 1, step):
        REUTERStokens.append(seq[i: i + seqlen])


# In[ ]:


print(sequencesREUTERS[12:20])
OOVcount[:10]
print(REUTERStokens[0:13])


# In[ ]:


REUTERSx = np.zeros((len(REUTERStokens), seqlen-1), dtype=int)
REUTERSy = np.zeros((len(REUTERStokens), 1), dtype=int)
print(REUTERSx.shape)
for i, seq in enumerate(REUTERStokens):
    REUTERSx[i] = seq[:seqlen - 1]
    REUTERSy[i] = seq[-1]


# In[ ]:


REUTERSm = int(len(REUTERSy)*0.7)
print(REUTERSm)
REUTERS_xtest = REUTERSx[:REUTERSm]
REUTERS_ytest = REUTERSy[:REUTERSm]
REUTERS_xtrain = REUTERSx[REUTERSm:]
REUTERS_ytrain = REUTERSy[REUTERSm:]


# In[ ]:


def REUTERSlstm_model(sequence_length, vocab_size, layer_size):
    model = models.Sequential()
    model.add(layers.Embedding(vocab_size, layer_size))
    model.add(layers.Dropout(0.4))
    model.add(layers.LSTM(layer_size))    
    model.add(layers.Dropout(0.4))
    model.add(layers.Dense(vocab_size, activation='softmax'))
    return model


# In[ ]:


print("check")
REUTERSmodel = REUTERSlstm_model(seqlen, 5000, 32)
optimizer = optimizers.Adam(learning_rate=0.01)
REUTERSmodel.compile(loss='sparse_categorical_crossentropy', optimizer=optimizer)


# In[ ]:


historyREUTERS = REUTERSmodel.fit(REUTERS_xtrain, REUTERS_ytrain,
             validation_data=(REUTERS_xtest, REUTERS_ytest),
             epochs=5,
             verbose=1)


# In[ ]:


plt.plot(historyREUTERS.history['loss'])
plt.plot(historyREUTERS.history['val_loss'])


# In[ ]:


tokenizerREUTERS.word_index


# In[ ]:


REUTERSs = tokenizerREUTERS.texts_to_sequences(["American secrets exposed"])
REUTERSinput = np.array(REUTERSs)
for i in range(20):
    pred = REUTERSmodel.predict(REUTERSinput)
    maxREUTERS = np.argmax(pred)
    
    print(tokenizerREUTERS.sequences_to_texts([[maxREUTERS]]))
    REUTERSinput = np.append(REUTERSinput[0,1:], maxREUTERS).reshape(1,seqlen-1)


# ***BuzzFeed***

# In[ ]:


BuzzFeed = df[df["publication"] == "Buzzfeed News"]
BuzzFeed = BuzzFeed["title"]
print(BuzzFeed)


# In[ ]:


plt.hist([len(title.split()) for title in BuzzFeed])


# In[ ]:


BuzzFeedwords = []
for title in BuzzFeed:
    title = title + " ENDLENDL"
    #add an end token to each of the titles
    BuzzFeedwords.append(title)
    
# init the tokenizer with a out_of_vocabulary token 
tokenizerBuzzFeed = Tokenizer(oov_token="<OOV>")
tokenizerBuzzFeed.num_words = maxwords
# generate word indexes
tokenizerBuzzFeed.fit_on_texts(BuzzFeedwords)

# generate sequences and apply padding
sequencesBuzzFeed = tokenizerBuzzFeed.texts_to_sequences(BuzzFeedwords)
#TODO: reduce vocab to 10k 
BuzzFeedset = set(i for j in sequencesBuzzFeed for i in j)


# In[ ]:


tokenizerBuzzFeed.word_index


# In[ ]:


len(BuzzFeedset)


# In[ ]:


OOVcount = []
for seq in sequencesBuzzFeed:
    OOVcount.append( list(seq).count(1) )


# In[ ]:


OOVcount.count(0)


# In[ ]:


print(len(OOVcount))


# In[ ]:


seqlen = 4
step = 1
BuzzFeedtokens = []

for x, seq in enumerate(sequencesBuzzFeed):
    if(OOVcount[x] > 0):
        continue
    if(len(seq)<seqlen + 1):
        continue
    for i in range(0, len(seq) - seqlen + 1, step):
        BuzzFeedtokens.append(seq[i: i + seqlen])


# In[ ]:


print(sequencesBuzzFeed[5:10])
BuzzFeedtokens[:10]


# In[ ]:


BuzzFeedx = np.zeros((len(BuzzFeedtokens), seqlen-1), dtype=int)
BuzzFeedy = np.zeros((len(BuzzFeedtokens), 1), dtype=int)
print(BuzzFeedx.shape)
for i, seq in enumerate(BuzzFeedtokens):
    BuzzFeedx[i] = seq[:seqlen - 1]
    BuzzFeedy[i] = seq[-1]


# In[ ]:


BuzzFeedm = int(len(BuzzFeedy)*0.7)
print(BuzzFeedm)
BuzzFeed_xtest = BuzzFeedx[:BuzzFeedm]
BuzzFeed_ytest = BuzzFeedy[:BuzzFeedm]
BuzzFeed_xtrain = BuzzFeedx[BuzzFeedm:]
BuzzFeed_ytrain = BuzzFeedy[BuzzFeedm:]


# In[ ]:


def BuzzFeedlstm_model(sequence_length, vocab_size, layer_size):
    model = models.Sequential()
    model.add(layers.Embedding(vocab_size, layer_size))
    model.add(layers.Dropout(0.4))
    model.add(layers.LSTM(layer_size))    
    model.add(layers.Dropout(0.4))
    model.add(layers.Dense(vocab_size, activation='softmax'))
    return model


# In[ ]:


BuzzFeedmodel = BuzzFeedlstm_model(seqlen, len(BuzzFeedset), 32)
optimizer = optimizers.Adam(learning_rate=0.01)
BuzzFeedmodel.compile(loss='sparse_categorical_crossentropy', optimizer=optimizer)


# In[ ]:


historyBuzzFeed = BuzzFeedmodel.fit(BuzzFeed_xtrain, BuzzFeed_ytrain,
             validation_data=(BuzzFeed_xtest, BuzzFeed_ytest),
             epochs=10,
             verbose=1)


# In[ ]:


plt.plot(historyBuzzFeed.history['loss'])
plt.plot(historyBuzzFeed.history['val_loss'])


# In[ ]:


tokenizerBuzzFeed.word_index


# In[ ]:


BuzzFeeds = tokenizerBuzzFeed.texts_to_sequences(["Americans exposed to"])
BuzzFeedinput = np.array(BuzzFeeds)
for i in range(10):
    pred = BuzzFeedmodel.predict(BuzzFeedinput)
    #print(tokenizerBuzzFeed.sequences_to_texts([0]))
    maxBuzzFeed = np.argmax(pred)
    print(tokenizerBuzzFeed.sequences_to_texts([[maxBuzzFeed]]))
    BuzzFeedinput = np.append(BuzzFeedinput[0,1:], maxBuzzFeed).reshape(1,seqlen-1)


# ***PAST ATTEMPTS:***

# **TRAINING TMZ**

# In[ ]:


TMZs = tokenizerTMZ.texts_to_sequences(["Donald Trump has been"])
TMZinput = np.array(TMZs)
for i in range(10):
    print(TMZinput)
    pred = TMZmodel.predict(TMZinput)
    maxTMZ = np.argmax(pred)
    print(tokenizerTMZ.sequences_to_texts([[maxTMZ]]))
    TMZinput = np.append(TMZinput[0,1:], maxTMZ).reshape(1,seqlen-1)


# In[ ]:


maxTMZ = np.argmax(pred)
maxTMZ


# In[ ]:


tokenizerTMZ.sequences_to_texts([[maxTMZ]])


# In[ ]:


tokenizerTMZ.word_index


# **TRAINING FOX**

# In[ ]:


print(Foxwords[:5])


# In[ ]:


seqlen = 5
step = 3
Foxtokens = []
#TMZf = paddedTMZ[:10000].flatten()
for seq in Foxpadded:
    for i in range(0, len(seq) - seqlen - 1, step):
        Foxtokens.append(seq[i: i + seqlen])


# In[ ]:


print(len(Foxpadded))
len(Foxtokens)


# In[ ]:


Foxx = np.zeros((len(Foxtokens), seqlen-1), dtype=int)
Foxy = np.zeros((len(Foxtokens), 1), dtype=int)
print(Foxx.shape)
for i, seq in enumerate(Foxtokens):
    Foxx[i] = seq[:seqlen - 1]
    Foxy[i] = seq[-1]


# In[ ]:


print(Foxtokens[0:6])
print(Foxx[1])
print(Foxy[1])


# In[ ]:


Foxm = int(len(Foxy)*0.7)
print(Foxm)
Fox_xtest = Foxx[:Foxm]
Fox_ytest = Foxy[:Foxm]
Fox_xtrain = Foxx[Foxm:]
Fox_ytrain = Foxy[Foxm:]


# In[ ]:


def Foxlstm_model(sequence_length, vocab_size, layer_size):
    model = models.Sequential()
    model.add(layers.Embedding(vocab_size, layer_size))
    model.add(layers.Dropout(0.2))
    model.add(layers.LSTM(layer_size))    
    model.add(layers.Dropout(0.2))
    model.add(layers.Dense(vocab_size, activation='softmax'))
    return model


# In[ ]:


Foxmodel = Foxlstm_model(seqlen, len(Foxset), 32)
optimizer = optimizers.RMSprop(learning_rate=0.01)
Foxmodel.compile(loss='sparse_categorical_crossentropy', optimizer=optimizer)


# In[ ]:


print(Fox_xtrain[:5])
print(Fox_ytrain[:5])


# In[ ]:


Foxmodel.fit(Fox_xtrain, Fox_ytrain,
             validation_data=(Fox_xtest, Fox_ytest),
             epochs=20,
             verbose=1)


# In[ ]:


Foxs = Foxtokenizer.texts_to_sequences(["Trump is going to"])
Foxinput = np.array(Foxs)
for i in range(10):
    print(Foxinput)
    pred = Foxmodel.predict(Foxinput)
    maxFox = np.argmax(pred)
    print(Foxtokenizer.sequences_to_texts([[maxFox]]))
    Foxinput = np.append(Foxinput[0,1:], maxFox).reshape(1,seqlen-1)


# In[ ]:


maxFox = np.argmax(pred)
maxFox


# In[ ]:


Foxtokenizer.word_index


# **TRAINING NYT** 

# In[ ]:


print(NYTwords[:5])


# In[ ]:


seqlen = 5
step = 3
NYTtokens = []
#TMZf = paddedTMZ[:10000].flatten()
for seq in NYTpadded[:10000]:
    for i in range(0, len(seq) - seqlen - 1, step):
        NYTtokens.append(seq[i: i + seqlen])


# In[ ]:


NYTx = np.zeros((len(NYTtokens), seqlen-1), dtype=int)
NYTy = np.zeros((len(NYTtokens), 1), dtype=int)
print(NYTx.shape)
for i, seq in enumerate(NYTtokens):
    NYTx[i] = seq[:seqlen - 1]
    NYTy[i] = seq[-1]


# In[ ]:


print(len(NYTpadded))
len(NYTtokens)


# In[ ]:


NYTm = int(len(NYTy)*0.7)
print(NYTm)
NYT_xtest = NYTx[:Foxm]
NYT_ytest = NYTy[:Foxm]
NYT_xtrain = NYTx[Foxm:]
NYT_ytrain = NYTy[Foxm:]


# In[ ]:


def Foxlstm_model(sequence_length, vocab_size, layer_size):
    model = models.Sequential()
    model.add(layers.Embedding(vocab_size, layer_size))
    model.add(layers.LSTM(layer_size))    
    model.add(layers.Dropout(0.3))
    model.add(layers.Dense(vocab_size, activation='softmax'))
    return model


# In[ ]:


Foxmodel = Foxlstm_model(seqlen, len(Foxset), 32)
optimizer = optimizers.RMSprop(learning_rate=0.01)
Foxmodel.compile(loss='sparse_categorical_crossentropy', optimizer=optimizer)


# In[ ]:




